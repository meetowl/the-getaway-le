package FrontEnd;

import BackEnd.JukeboxHelper;
import  javafx.application.Application;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaException;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

import static BackEnd.JukeboxHelper.*;

/***
 * FrontEnd.Main class for this app, starts the window and opens up the 'Start' window
 * @author Christian Sanger
 *
 */

//this is my push <3
//calum was here

public class Main extends Application {
    //These variables are meant to be used throughout the program, I know encapsulation and all that however sometimes it's not feasible
    //These are used mostly for the mediaPlayer and Jukebox methods as those have to be available throughout the whole program always

    private static final double DEFAULT_SOUND_LEVEL = 10.0;
    private static int track;

    @Override
    public void start(Stage primaryStage) throws FileNotFoundException {
        WindowLoader wl = new WindowLoader(primaryStage);
        HashMap<String, String> initData;initData = new HashMap<>();
        Scanner in;
        //  Default settings if no config file.
        double backgroundVol;
        double sfxVol;
        try {
            File configFile = new File("SaveData/config.txt");
            in = new Scanner(configFile);
            backgroundVol = Double.parseDouble(in.next());
            sfxVol = Double.parseDouble(in.next());

            JukeBoxController.currentlyPlaying = in.next();
            in.close();
        } catch (NumberFormatException e) {
            System.out.println("Config file incorrect format:" + e.getMessage());
            backgroundVol = DEFAULT_SOUND_LEVEL;
            sfxVol = DEFAULT_SOUND_LEVEL;
        } catch  (FileNotFoundException e) {
            System.out.println("Config not found, using defaults");
            backgroundVol = DEFAULT_SOUND_LEVEL;
            sfxVol = DEFAULT_SOUND_LEVEL;
        }

        primaryStage.setFullScreen(true);
        primaryStage.setTitle("The Getaway");
        primaryStage.getIcons().add(new Image("logo.png"));
        primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);

        playMusic();

        initData.put("BackgroundVol", String.valueOf(backgroundVol));
        initData.put("SFXVol", String.valueOf(sfxVol / 100));

        wl.load("StartScreen", initData);
        primaryStage.show();
    }

    /**
     * This method goes through a playlist and and plays each song after the previous ones finished.
     */
    public static void playMusic() throws FileNotFoundException {
        Scanner in;
        try {
            in = new Scanner(new File("SaveData/config.txt"));
        }catch (FileNotFoundException e)    {
            File config = new File("SaveData/config.txt");
            in = new Scanner(new File("SaveData/config.txt"));
        }

        in.useDelimiter(" ");
        //Setting these as variables because I came up with the default song idea later (So it's at the end of the file) and changing the config format was a bit of a hassle
        //Another thing to note is that for some reason, near the end of the project, Java decided it was gonna throw me an InputMismatch
        //When trying to use in.nextDouble, but it works if I parse the String as a double? Who knows
        double musicVol = Double.parseDouble(in.next());
        double sfxvol = Double.parseDouble(in.next());

        sound = new Media(new File("Assets/Music/" + in.next()).toURI().toString());
        try {
            JukeboxHelper.mediaPlayer = new MediaPlayer(sound);
        } catch (MediaException e) {
            if (System.getProperty("os.name").equals("Linux")) {
                System.err.println("error: application requires ffmpeg version 54-57");
            } else {
                System.err.println(e.getMessage());
            }
            System.exit(1);

        }
        JukeboxHelper.mediaPlayer.setCycleCount(0);

        JukeboxHelper.mediaPlayer.setVolume(musicVol / 100);
        JukeboxHelper.SFXVol = (sfxvol / 100);

        JukeboxHelper.mediaPlayer.play();
        JukeboxHelper.mediaPlayer.setOnEndOfMedia(new Runnable() {
            @Override
            public void run() {
                try {
                    playMusic();
                } catch (FileNotFoundException e) {
                    System.out.println("I'm pretty sure this can never happen");
                }
            }
        });
    }

    /***
     * Only starts javaFX
     * @param args doesn't use any arguments right now.
     */

    public static void main(String[] args) {
        launch(args);
    }

}
