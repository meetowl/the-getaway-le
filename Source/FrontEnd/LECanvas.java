package FrontEnd;

import BackEnd.*;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.transform.Rotate;

import java.io.File;
import java.io.IOException;

import static BackEnd.TileType.*;

/**
 * LECanvas class is used to interact with the canvas(also reffered as render) on the Level Editor view.
 * The LECanvas enables to draw the board when required to display the editted level to the user.
 * Methods various methods available such as loading the board into the canvas, resizing the board and etc.
 *
 * @author Adilet Eshimkanov
 * @author Tom Cresswell
 * @author Pat Sambor
 * @version 1.0
 */
public class LECanvas {



    Image imageEmpty;
    Image imageGoal;
    Image imageStraight;
    Image imageTShape;
    Image imageCorner;

    Image imagePlGrey;
    Image imagePlPink;
    Image imagePlBlack;
    Image imagePlWhite;
    Image imagePlOrange;
    Image imagePlYellow;
    Image imagePlBurgundy;
    Image imagePlGlaucous;
    Image imagePlTurquiose;
    Image imagePlAquamarine;

    private final ResizableCanvas canvas;

    Slot[][] arrayBoard = new Slot[9][9];
    double tileSize;
    Label lblstatus;

    int topLeftX = 0;
    int topLeftY = 0;
    int topRightX = 0;
    int boardSizePx;
    boolean widthCentre;


    Player player1; // (Tom) only made to help getSlotInfo method, change if needed.
    Player player2;
    Player player3;
    Player player4;

    PlaceCoords player1Coords = new PlaceCoords(0,0);
    PlaceCoords player2Coords = new PlaceCoords(0,0);
    PlaceCoords player3Coords = new PlaceCoords(0,0);
    PlaceCoords player4Coords = new PlaceCoords(0,0);

    CarColours playerColor1;
    CarColours playerColor2;
    CarColours playerColor3;
    CarColours playerColor4;
    TileType tileTemp;
    private File slotInfoFile;

    //variables for tile selected if there to swap with below tile.
    TileType tempTileType;
    int tempTileTypeRotation;
    int tempTileX = 99; // refers to empty/not set
    int tempTileY = 99;

    //variables for the tile swap, below being under the mouse if a tile is selected.
    TileType belowTileType;
    int belowTileTypeRotation;
    int belowTileX;
    int belowTileY;

    //to check if a swap is needed(a tile is taken or not)
    boolean prevTileTaken = false;


    //A boolean to load unselected variants the first time around
    boolean boolInitialising = true;

    Image rotationArrow = new Image("rotateArrowLE.png");

    //Storing the width and height
    int width;
    int height;
    /**
     * Constructor for LECanvas to intialise the canvas and label.
     *
     * @param impCanvas canvas for rendering the board
     * @param lblStatus label for status updates
     */
    public LECanvas(ResizableCanvas impCanvas, Label lblStatus, int boardWidth, int boardHeight) {
        canvas = impCanvas;
        this.lblstatus = lblStatus;

        imageEmpty = new Image("empty.png");
        imageGoal = new Image("goal.png");
        imageStraight = new Image("straight.png");
        imageTShape = new Image("t_shape.png");
        imageCorner = new Image("corner.png");

        imagePlGrey = new Image("playerGREY.png");
        imagePlPink = new Image("playerPINK.png");
        imagePlBlack = new Image("playerBLACK.png");
        imagePlWhite = new Image("playerWHITE.png");
        imagePlOrange = new Image("playerORANGE.png");
        imagePlYellow = new Image("playerYELLOW.png");
        imagePlBurgundy = new Image("playerBURGUNDY.png");
        imagePlGlaucous = new Image("playerGLAUCOUS.png");
        imagePlTurquiose = new Image("playerTURQUOISE.png");
        imagePlAquamarine = new Image("playerAQUAMARINE.png");

        width = boardWidth;
        height = boardHeight;
    }

    /**
     * Method to load in a board.
     *
     * @param loadArrayBoard saved board to load
     * @param width          width of the board
     * @param height         height of the board
     * @param player1CoordsLoad  Player 1 coordinates.
     * @param player2CoordsLoad  Player 2 coordinates.
     * @param player3CoordsLoad  Player 3 coordinates.
     * @param player4CoordsLoad  Player 4 coordinates.
     * @param playerColor1   Player 1 colour.
     * @param playerColor2   Player 2 colour.
     * @param playerColor3   Player 3 colour.
     * @param playerColor4   Player 4 colour.
     */
    public void loadBoard(Slot[][] loadArrayBoard, int width, int height, PlaceCoords player1CoordsLoad,
                          PlaceCoords player2CoordsLoad, PlaceCoords player3CoordsLoad, PlaceCoords player4CoordsLoad,
                          CarColours playerColor1, CarColours playerColor2, CarColours playerColor3, CarColours playerColor4) {

        //loading board
        arrayBoard = loadArrayBoard;
        setWidth(width);
        setHeight(height);
        this.playerColor1 = playerColor1;
        this.playerColor2 = playerColor2;
        this.playerColor3 = playerColor3;
        this.playerColor4 = playerColor4;

        if (canvas.getHeight() >= canvas.getWidth()) {
            boardSizePx = (int) canvas.getWidth();
            topLeftY = (int) (canvas.getHeight() - boardSizePx) / 2;
        } else if (canvas.getHeight() <= canvas.getWidth()) {
            boardSizePx = (int) canvas.getHeight();
            topLeftX = (int) (canvas.getWidth() - boardSizePx) / 2;
            topRightX = (int) ((canvas.getWidth() - boardSizePx) / 2 + boardSizePx);
        }

        for (int i = width; i <= 8; i++) {
            for (int j = height; j <= 8; j++) {
                Slot slot = new Slot(0, TileType.EMPTY, i, j, 0);
                arrayBoard[i][j] = slot;
                System.out.println("Placed a new slot in arrayBoard " + i + " " + j);
            }
        }

        //intermediary values
        Slider sliWidth = new Slider();
        Slider sliHeight = new Slider();
        sliWidth.setValue(width);
        sliHeight.setValue(height);

        //check what is the max height and width and use the highest.
        if (sliHeight.getValue() == 9 || sliWidth.getValue() == 9) {
            tileSize = boardSizePx / 9;
            System.out.println("9x9 board");
        } else if (sliHeight.getValue() == 8 || sliWidth.getValue() == 8) {
            tileSize = boardSizePx / 8;
        } else if (sliHeight.getValue() == 7 || sliWidth.getValue() == 7) {
            tileSize = boardSizePx / 7;
        } else if (sliHeight.getValue() == 6 || sliWidth.getValue() == 6) {
            tileSize = boardSizePx / 6;
        } else if (sliHeight.getValue() == 5 || sliWidth.getValue() == 5) {
            tileSize = boardSizePx / 5;
        } else if (sliHeight.getValue() == 4 || sliWidth.getValue() == 4) {
            tileSize = boardSizePx / 4;
        } else if (sliHeight.getValue() == 3 || sliWidth.getValue() == 3) {
            tileSize = boardSizePx / 3;
        }


        drawBoard(sliWidth, sliHeight, tileSize);
        //adding players
        arrayBoard[player1CoordsLoad.getLocX()][player1CoordsLoad.getLocY()].setWhichPlayer(1);
        arrayBoard[player1CoordsLoad.getLocX()][player1CoordsLoad.getLocY()].setCarColours(playerColor1);
        drawPlayer(player1CoordsLoad.getLocX()+1, player1CoordsLoad.getLocY()+1, playerColor1, 0);

        arrayBoard[player2CoordsLoad.getLocX()][player2CoordsLoad.getLocY()].setWhichPlayer(2);
        arrayBoard[player2CoordsLoad.getLocX()][player2CoordsLoad.getLocY()].setCarColours(playerColor2);
        drawPlayer(player2CoordsLoad.getLocX()+1, player2CoordsLoad.getLocY()+1, playerColor2, 0);

        arrayBoard[player3CoordsLoad.getLocX()][player3CoordsLoad.getLocY()].setWhichPlayer(3);
        arrayBoard[player3CoordsLoad.getLocX()][player3CoordsLoad.getLocY()].setCarColours(playerColor3);
        drawPlayer(player3CoordsLoad.getLocX()+1, player3CoordsLoad.getLocY()+1, playerColor3, 0);

        arrayBoard[player4CoordsLoad.getLocX()][player4CoordsLoad.getLocY()].setWhichPlayer(4);
        arrayBoard[player4CoordsLoad.getLocX()][player4CoordsLoad.getLocY()].setCarColours(playerColor4);
        drawPlayer(player4CoordsLoad.getLocX()+1, player4CoordsLoad.getLocY()+1, playerColor4, 0);

        boolInitialising = false;
    }

    /**
     * Method to set the initial empty board for Level Editor.
     *
     * @param sliHeight slider of the height
     * @param sliWidth  slider of the width
     */
    public void setEmptyBoard(Slider sliHeight, Slider sliWidth) {

        setWidth(width);
        setHeight(height);
        //find if the height or the width is bigger to align the render to the centre and
        //determine top left corner of the board for drawing tiles
        if (canvas.getHeight() >= canvas.getWidth()) {
            boardSizePx = (int) canvas.getWidth();
            topLeftY = (int) (canvas.getHeight() - boardSizePx) / 2;
        } else if (canvas.getHeight() <= canvas.getWidth()) {
            boardSizePx = (int) canvas.getHeight();
            topLeftX = (int) (canvas.getWidth() - boardSizePx) / 2;
            topRightX = (int) ((canvas.getWidth() - boardSizePx) / 2 + boardSizePx);
        }

        //check what is the max height and width and use the highest.
        if (sliHeight.getValue() == 9 || sliWidth.getValue() == 9) {
            tileSize = boardSizePx / 9;
            System.out.println("9x9 board");
        } else if (sliHeight.getValue() == 8 || sliWidth.getValue() == 8) {
            tileSize = boardSizePx / 8;
        } else if (sliHeight.getValue() == 7 || sliWidth.getValue() == 7) {
            tileSize = boardSizePx / 7;
        } else if (sliHeight.getValue() == 6 || sliWidth.getValue() == 6) {
            tileSize = boardSizePx / 6;
        } else if (sliHeight.getValue() == 5 || sliWidth.getValue() == 5) {
            tileSize = boardSizePx / 5;
        } else if (sliHeight.getValue() == 4 || sliWidth.getValue() == 4) {
            tileSize = boardSizePx / 4;
        } else if (sliHeight.getValue() == 3 || sliWidth.getValue() == 3) {
            tileSize = boardSizePx / 3;
        }

        for (int i = 0; i <= 8; i++) {
            for (int j = 0; j <= 8; j++) {

                Slot slot = new Slot(0, TileType.EMPTY, i, j, 0);
                arrayBoard[i][j] = slot;

                System.out.println("Placed a new slot in arrayBoard " + i + " " + j);
            }
        }

        drawBoard(sliWidth, sliHeight, tileSize);

        //check the values for arrayboard in regards to 1-9

        arrayBoard[0][0].setWhichPlayer(1);
        arrayBoard[0][0].setCarColours(CarColours.PINK);
        player1Coords.setLocX(1);
        player1Coords.setLocY(1);
        drawPlayer(1, 1, arrayBoard[0][0].getCarColours(), 0);
        System.out.println("Player at " + 0 + " " + 0 + " " + arrayBoard[0][0].getWhichPlayer());

        arrayBoard[(int) sliWidth.getValue() - 1][0].setWhichPlayer(2);
        arrayBoard[(int) sliWidth.getValue() - 1][0].setCarColours(CarColours.YELLOW);
        player3Coords.setLocX((int) sliWidth.getValue());
        player3Coords.setLocY(1);
        drawPlayer((int) sliWidth.getValue(), 1, arrayBoard[(int) sliWidth.getValue() - 1][0].getCarColours(), 0);
        System.out.println("Player at " + (int) sliWidth.getValue() + " " + 0 + " " + arrayBoard[(int) sliWidth.getValue() - 1][0].getWhichPlayer());

        arrayBoard[0][(int) sliHeight.getValue() - 1].setWhichPlayer(3);
        arrayBoard[0][(int) sliHeight.getValue() - 1].setCarColours(CarColours.TURQUOISE);
        player2Coords.setLocX(1);
        player2Coords.setLocY((int) (sliHeight.getValue()));
        drawPlayer(1, (int) sliHeight.getValue(), arrayBoard[0][(int) sliHeight.getValue() - 1].getCarColours(), 0);
        System.out.println("Player at " + 0 + " " + sliHeight.getValue() + " " + arrayBoard[0][(int) sliHeight.getValue() - 1].getWhichPlayer());

        arrayBoard[(int) sliWidth.getValue()-1][(int) sliHeight.getValue()-1].setWhichPlayer(4);
        arrayBoard[(int) sliWidth.getValue()-1][(int) sliHeight.getValue()-1].setCarColours(CarColours.ORANGE);
        player4Coords.setLocX((int) sliWidth.getValue());
        player4Coords.setLocY((int) sliHeight.getValue());
        drawPlayer((int) sliWidth.getValue(), (int) sliHeight.getValue(),
                arrayBoard[(int) sliWidth.getValue()-1][(int) sliHeight.getValue()-1].getCarColours(), 0);
        System.out.println("Player at " + sliWidth.getValue() + " " + sliHeight.getValue() + " " + arrayBoard[(int) sliWidth.getValue() - 1][(int) sliHeight.getValue() - 1].getWhichPlayer());
        playerColor1 = CarColours.PINK;
        playerColor2 = CarColours.YELLOW;
        playerColor3 = CarColours.TURQUOISE;
        playerColor4 = CarColours.ORANGE;

        boolInitialising = false;

    }

    /**
     * Method to resize the board, when the sliders are updated.
     *
     * @param width  new value of the slider for width
     * @param height new value of the slider for height
     */
    public void resizeBoard(int width, int height, CarColours enumColor1, CarColours enumColor2, CarColours enumColor3, CarColours enumColor4, Slider sliWidth, Slider sliHeight ) {
        canvas.draw();
        setWidth(width);
        setHeight(height);
        sliWidth.setValue(width);
        sliHeight.setValue(height);

        //check what is the max height and width and use the highest.
        if (sliHeight.getValue() == 9 || sliWidth.getValue() == 9) {
            tileSize = boardSizePx / 9;
            System.out.println("9x9 board");
        } else if (sliHeight.getValue() == 8 || sliWidth.getValue() == 8) {
            tileSize = boardSizePx / 8;
        } else if (sliHeight.getValue() == 7 || sliWidth.getValue() == 7) {
            tileSize = boardSizePx / 7;
        } else if (sliHeight.getValue() == 6 || sliWidth.getValue() == 6) {
            tileSize = boardSizePx / 6;
        } else if (sliHeight.getValue() == 5 || sliWidth.getValue() == 5) {
            tileSize = boardSizePx / 5;
        } else if (sliHeight.getValue() == 4 || sliWidth.getValue() == 4) {
            tileSize = boardSizePx / 4;
        } else if (sliHeight.getValue() == 3 || sliWidth.getValue() == 3) {
            tileSize = boardSizePx / 3;
        }
        boolInitialising = true;
        drawBoard(sliWidth, sliHeight, tileSize);
        boolInitialising = false;
        for (int i = 0; i <= 8; i++) {
            for (int j = 0; j <= 8; j++) {

                arrayBoard[i][j].setWhichPlayer(0);
                arrayBoard[i][j].setCarColours(null);

            }
        }

        arrayBoard[0][0].setWhichPlayer(1);
        arrayBoard[0][0].setCarColours(enumColor1);
        drawPlayer(1,1,enumColor1,0);

        arrayBoard[width-1][0].setWhichPlayer(2);
        arrayBoard[width-1][0].setCarColours(enumColor2);
        drawPlayer(width,1,enumColor2,0);

        arrayBoard[0][height-1].setWhichPlayer(3);
        arrayBoard[0][height-1].setCarColours(enumColor3);
        drawPlayer(1,height,enumColor3,0);

        arrayBoard[width-1][height-1].setWhichPlayer(4);
        arrayBoard[width-1][height-1].setCarColours(enumColor4);
        drawPlayer(width,height,enumColor4,0);

    }

    /**
     * Method to draw a player at a specific coordinate on canvas from top left (0,0) origin.
     *
     * @param locX      location X of where to draw.
     * @param locY      location Y of where to draw.
     * @param carColour specified colour of the car to draw.
     */
    public void drawPlayer(int locX, int locY, CarColours carColour, int rotate) {

        System.out.println("drawPlayer method called");

        //check which car color image to draw
        Image image;

        switch (carColour) {
            case GREY:
                image = imagePlGrey;
                break;
            case PINK:
                image = imagePlPink;
                break;
            case BLACK:
                image = imagePlBlack;
                break;
            case WHITE:
                image = imagePlWhite;
                break;
            case ORANGE:
                image = imagePlOrange;
                break;
            case YELLOW:
                image = imagePlYellow;
                break;
            case BURGUNDY:
                image = imagePlBurgundy;
                break;
            case GLAUCOUS:
                image = imagePlGlaucous;
                break;
            case TURQUOISE:
                image = imagePlTurquiose;
                break;
            case AQUAMARINE:
                image = imagePlAquamarine;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + carColour);
        }

        System.out.println("Color of the car set.");

        double whereXDrawn = ((locX * tileSize) - tileSize) + topLeftX;
        double whereYDrawn = ((locY * tileSize) - tileSize) + topLeftY;
        System.out.println(topLeftX);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.save();
        rotateMethod(gc, rotate, whereXDrawn + tileSize / 2, whereYDrawn + tileSize / 2);
        System.out.println("carImage rotation is set");

        gc.drawImage(image, whereXDrawn, whereYDrawn, tileSize, tileSize);

        System.out.println("Car needed to be draw at " + locX + " " + locY);
        System.out.println("Drawn a car at " + whereXDrawn + " " + whereYDrawn);

        gc.restore();
    }

    /**
     * Method to draw the board on the canvas
     *
     * @param sliWidth  slider for width
     * @param sliHeight slider for height
     * @param tileSize  tile size on the canvas
     */
    public void drawBoard(Slider sliWidth, Slider sliHeight, double tileSize) {
        setWidth((int) sliWidth.getValue());
        setHeight((int) sliHeight.getValue());
        /*Image background = new Image("LEBackground.jpg");
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.drawImage(background, 0, 0, canvas.getWidth(), canvas.getHeight());*/
        for (int i = 0; i < sliWidth.getValue(); i++) {

            for (int j = 0; j < sliHeight.getValue(); j++) {
                int locX = i + 1;
                int locY = j + 1;
                /*if (height > width) {
                    locX = locX + w idth;
                }else if(width > height)    {
                    locY = locY + height;
                }*/
                drawOnCanvas(arrayBoard[i][j].getTileType(), locX, locY, arrayBoard[i][j].getRotate());

                System.out.println("Placed a new slot in arrayBoard " + i + " " + j);
            }
        }

    }

    public void CanvasDraw() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        //gc.setFill(Color.BLUE);
        //gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    //Brilliant but lazy
    public void CanvasDrawPizza() {
        Image pizza = new Image("pizzaMain.png");
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.drawImage(pizza, 0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * Method to save the player and tile info from the slot to a file
     *
     * @throws IOException issue with file
     */

    /*
    public void getSlotInfo(Slot slot) throws IOException {
        //int whichPlayer = slot.setWhichPlayer(0);
        tileTemp = slot.getTileType();

        slotInfoFile = new File("Assets/SlotInfoFile.txt");

        FileWriter writer = new FileWriter(slotInfoFile);
        writer.write(which.toString() + tileTemp.toString());
        writer.flush();
        writer.close();
    }
     */
    public Slot[][] getArrayBoard() {
        return arrayBoard;
    }

    public void getAllVariable() {

    }

    public void setPlayerColor1(CarColours playerColor1) {
        this.playerColor1 = playerColor1;
    }

    public void setPlayerColor2(CarColours playerColor2) {
        this.playerColor2 = playerColor2;
    }

    public void setPlayerColor3(CarColours playerColor3) {
        this.playerColor3 = playerColor3;
    }

    public void setPlayerColor4(CarColours playerColor4) {
        this.playerColor4 = playerColor4;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    /**
     * Method to set a tile on a clicked location
     *
     * @param mouseX       x coordinate of the mouse location
     * @param mouseY       y coordinate of the mouse location
     * @param tempSel      the tile value to be set
     * @param sliWidth     slider for width
     * @param sliHeight    slider for height
     * @param tileSelected boolean to see if a tile is selected to place
     * @param intPlayerSel
     * @param rotate       value of the rotation of the tile
     */
    public String placeOnSlot(double mouseX, double mouseY, String tempSel, Slider sliWidth, Slider sliHeight, boolean tileSelected, int intPlayerSel, int rotate, LEMainController theController) {

        System.out.println("placeOnSlot method reached");
        //check if a player is being placed
        if (tempSel.equals("playerPINK") || tempSel.equals("playerYELLOW") || tempSel.equals("playerTURQUOISE") || tempSel.equals("playerORANGE") ||
                tempSel.equals("playerGREY") || tempSel.equals("playerBLACK") ||
                tempSel.equals("playerWHITE") || tempSel.equals("playerBURGUNDY") || tempSel.equals("playerGLAUCOUS") ||
                tempSel.equals("playerAQUAMARINE")) {

            CarColours tempCarColour = null;
            String enumTempSel = StringTrimmer.trim(tempSel, "player");
            tempCarColour = CarColours.valueOf(enumTempSel.toUpperCase());


            PlaceCoords placeCoords = checkWhereToPlace((int) tileSize, mouseX, mouseY, sliWidth, sliHeight);
            setPlayerCoords();

            //With enough IF statements, I can do anything
            //This is a validation check to see whether you're trying to place a player onto another player
            //If it's the same player you click again you'll de-select
            if (placeCoords.getLocX() == player1Coords.getLocX() && placeCoords.getLocY() == player1Coords.getLocY()) {
                if (intPlayerSel == 1)   {
                    theController.onPlayer1();
                    checkForPlayerDraw(placeCoords);
                    highlighterMethod(mouseX, mouseY, sliWidth, sliHeight, null, 0, false, theController);
                    return "Select Player 1/RMB Rotates";
                }
                return "Can't place " + "P" + intPlayerSel + " on " + "P1";
            } else if (placeCoords.getLocX() == player2Coords.getLocX() && placeCoords.getLocY() == player2Coords.getLocY()) {
                if (intPlayerSel == 2)   {
                    theController.onPlayer2();
                    checkForPlayerDraw(placeCoords);
                    highlighterMethod(mouseX, mouseY, sliWidth, sliHeight, null, 0, false, theController);
                    return "Select Player 2/RMB Rotates";
                }
                return "Can't place " + "P" + intPlayerSel + " on " + "P2";
            } else if (placeCoords.getLocX() == player3Coords.getLocX() && placeCoords.getLocY() == player3Coords.getLocY()) {
                if (intPlayerSel == 3)   {
                    theController.onPlayer3();
                    checkForPlayerDraw(placeCoords);
                    highlighterMethod(mouseX, mouseY, sliWidth, sliHeight, null, 0, false, theController);
                    return "Select Player 3/RMB Rotates";
                }
                return "Can't place " + "P" + intPlayerSel + " on " + "P3";
            } else if (placeCoords.getLocX() == player4Coords.getLocX() && placeCoords.getLocY() == player4Coords.getLocY()) {
                if (intPlayerSel == 4)   {
                    theController.onPlayer4();
                    checkForPlayerDraw(placeCoords);
                    highlighterMethod(mouseX, mouseY, sliWidth, sliHeight, null, 0, false, theController);
                    return "Select Player 4/RMB Rotates";
                }
                return "Can't place " + "P" + intPlayerSel + " on " + "P4";
            } else {
                if (placeCoords.getLocX() - 1 == -1 || placeCoords.getLocY() - 1 == -1) {
                    return "Out of Bounds!";
                }
                for (int i = 0; i < sliWidth.getValue(); i++) {
                    for (int j = 0; j < sliHeight.getValue(); j++) {

                        if (arrayBoard[i][j].getWhichPlayer() == intPlayerSel) {
                            int locX = i + 1;
                            int locY = j + 1;
                            /*if (height > width) {
                                locX = locX + width;
                            }else if(width > height)    {
                                locY = locY + height;
                            }*/
                            arrayBoard[i][j].setCarColours(null);
                            arrayBoard[i][j].setWhichPlayer(0);

                            /*This is a weird case, we have to pretend to the drawOnCanvas method
                            That we're initialising even though we're not
                            Because otherwise it'll draw the selected variant of the previous tile
                            A smarter man would've turned drawOnCanvas into two methods to deal with the 2 cases
                            But no
                             */
                            boolInitialising = true;
                            drawOnCanvas(arrayBoard[i][j].getTileType(), locX, locY, arrayBoard[i][j].getRotate());
                            boolInitialising = false;
                        } else {
                            System.out.println();
                        }

                    }
                }
            }
            //placing a player into a slot needs to be done.
            arrayBoard[placeCoords.getLocX() - 1][placeCoords.getLocY() - 1].setWhichPlayer(intPlayerSel);
            arrayBoard[placeCoords.getLocX() - 1][placeCoords.getLocY() - 1].setCarColours(tempCarColour);

            drawPlayer(placeCoords.getLocX(), placeCoords.getLocY(), tempCarColour, rotate);

            if(intPlayerSel == 1)   {
                theController.onPlayer1();
                highlighterMethod(mouseX, mouseY, sliWidth, sliHeight, arrayBoard[placeCoords.getLocX()][placeCoords.getLocY()].getTileType().toString(), arrayBoard[placeCoords.getLocX()][placeCoords.getLocY()].getRotate(), false, theController);
            }else if (intPlayerSel == 2) {
                theController.onPlayer2();
                highlighterMethod(mouseX, mouseY, sliWidth, sliHeight, null, 0, false, theController);
            }else if (intPlayerSel == 3)    {
                theController.onPlayer3();
                highlighterMethod(mouseX, mouseY, sliWidth, sliHeight, null, 0, false, theController);
            }else if (intPlayerSel == 4)    {
                theController.onPlayer4();
                highlighterMethod(mouseX, mouseY, sliWidth, sliHeight, null, 0, false, theController);
            }

            return "P" + intPlayerSel + " placed at " + placeCoords.getLocX() + " " + placeCoords.getLocY();
        } else {
            //if a tile is being drawn then this else will execute

            //Convert the tempSel(String) to tileType(TileType)
            TileType tileType = null;

            switch (tempSel) {
                case "empty":
                    tileType = TileType.EMPTY;
                    break;
                case "straight":
                    tileType = STRAIGHT;
                    break;
                case "t_shape":
                    tileType = T_SHAPE;
                    break;
                case "goal":
                    tileType = GOAL;
                    break;
                case "corner":
                    tileType = TileType.CORNER;
                    System.out.println("Corner selected as TileType");
                    break;
                default:
                    lblstatus.setText("Select something!");
                    break;
            }


            //lblstatus.setText("Tile " + );

            //check if something is selected
            if (tileSelected) {
                System.out.println("tileSelected is true");

                //check the slot location from the mouse location
                PlaceCoords placeCoords = checkWhereToPlace((int) tileSize, mouseX, mouseY, sliWidth, sliHeight);
                if (!placeCoords.getOutOfBounds()) {
                    System.out.println("Mouse location to Slot location, is complete.");

                    //Slot slot = new Slot(null, null, 0,0,0);
                    //arrayBoard[1][1] = slot;

                    //place the selected tile into the according arrayBoard place.
                    try {
                        arrayBoard[placeCoords.getLocX() - 1][placeCoords.getLocY() - 1].setTileType(tileType);
                        arrayBoard[placeCoords.getLocX() - 1][placeCoords.getLocY() - 1].setRotate(rotate);
                    }catch (ArrayIndexOutOfBoundsException e ) {
                        canvas.setCursor(Cursor.DEFAULT);
                        return "Out of Bounds!";
                    }

                    System.out.println("Set the new tile type, at Slot location" + placeCoords.getLocX() + " " + placeCoords.getLocY());

                    //draw the tile
                    drawOnCanvas(tileType, placeCoords.getLocX(), placeCoords.getLocY(), rotate);
                    System.out.println("drawOnCanvas method called, after Slot setting.");
                    checkForPlayerDraw(placeCoords);

                    return tempSel + " placed at " + placeCoords.getLocX() + " " + placeCoords.getLocY();
                } else {
                    return "Out of Bounds!";
                }
            }
        }
        return null;
    }
    boolean boolRepeating = false;
    /**
     * Method to draw a tile on the canvas
     *
     * @param tempSel tile value to be drawn
     * @param locX    location x of the tile to be drawn on the board
     * @param locY    location y of the tile to be drawn on the board
     * @param rotate  rotation value of the tile
     */
    public void drawOnCanvas(TileType tempSel, int locX, int locY, int rotate) {

        System.out.println("drawOnCanvas method called");

        Image image;
        String imageStr;

        //This solution is really gross but I can't think of another way to do it
        //Basically checks if the game is being loaded for the first time
        //If it is then it'll load the unselected variants
        //If it isn't it'll load the selected ones (So that placing down doesn't have a weird de-selection effect)
        if (boolInitialising)   {
            switch (tempSel) {
                case EMPTY:
                    image = imageEmpty;
                    imageStr = "empty";
                    break;
                case CORNER:
                    image = imageCorner;
                    imageStr = "corner";
                    break;
                case T_SHAPE:
                    image = imageTShape;
                    imageStr = "t_shape";
                    break;
                case GOAL:
                    image = imageGoal;
                    imageStr = "goal";
                    break;
                case STRAIGHT:
                    image = imageStraight;
                    imageStr = "straight";
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + tempSel);
            }
        }else   {
            switch (tempSel.toString()) {
                case "EMPTY":
                    imageStr = "empty";
                    image = Assets.getSelected(imageStr);
                    break;
                case "CORNER":
                    imageStr = "corner";
                    image = Assets.getSelected(imageStr);
                    break;
                case "T_SHAPE":
                    imageStr = "t_shape";
                    image = Assets.getSelected(imageStr);
                    break;
                case "GOAL":
                    imageStr = "goal";
                    image = Assets.getSelected(imageStr);
                    break;
                case "STRAIGHT":
                    imageStr = "straight";
                    image = Assets.getSelected(imageStr);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + tempSel);
            }
        }
        prevImage = Assets.get(imageStr);
        //determine image from tile type

        double whereXDrawn = ((locX * tileSize) - tileSize) + topLeftX;
        double whereYDrawn = ((locY * tileSize) - tileSize) + topLeftY;

        //This code will hopefully one day run if the board is lopsided and compensante the display for that
        //Remember to put this same check wherever mouselocations are used!
        /*
        if (width > height) {
            double newTopLeft = topLeftY + (tileSize * (height - width) / 2) ;
            whereYDrawn = ((locY * tileSize) - tileSize) - newTopLeft;
        }else if (height > width)   {
            double newTopLeft = topLeftX + (tileSize * (width - height) / 2 );
            whereXDrawn = ((locX * tileSize) - tileSize) - newTopLeft;
        }*/



        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.save();
        rotateMethod(gc, rotate, whereXDrawn + tileSize / 2, whereYDrawn + tileSize / 2);
        System.out.println("Image rotation is set");

        gc.drawImage(image, whereXDrawn, whereYDrawn, tileSize, tileSize);

        System.out.println("Image " + imageStr + " is Drawn at " + whereXDrawn + " " + whereYDrawn);

        gc.restore();
    }

    /**
     * Intermediary method used to rotate the images on canvas before drawing.
     *
     * @param gc    Graphics context
     * @param angle angle of rotfation
     * @param px    location x to draw on
     * @param py    location y to draw on
     */
    public void rotateMethod(GraphicsContext gc, double angle, double px, double py) {
        Rotate r = new Rotate(angle, px, py);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    }

    /**
     * Intermediary method used to determine the location of where to place the tile according to mouse cursor location and the size of the board
     *
     * @param tileSize size of the tile on the board
     * @param mouseX   mouse location x
     * @param mouseY   mouse location y
     * @param sliderW  slider for width
     * @param sliderH  slider for height
     * @return value returned for getting coordinates of the location of where to place the tile on board.
     */
    private PlaceCoords checkWhereToPlace(int tileSize, double mouseX, double mouseY, Slider sliderW, Slider sliderH) {

        PlaceCoords placeCoordsTemp = new PlaceCoords(0, 0);

        double pX = mouseX;
        double pY = mouseY;
        //checking for out of bounds and tile location
        if (pY < topLeftY || pX < topLeftX) {
            placeCoordsTemp.setOutOfBounds(true);
        } else {
            if (pX <= (tileSize * 1) + topLeftX) {
                placeCoordsTemp.setLocX(1);
            } else if (pX <= (tileSize * 2) + topLeftX) {
                placeCoordsTemp.setLocX(2);
            } else if (pX <= (tileSize * 3) + topLeftX && sliderW.getValue() > 2) {
                placeCoordsTemp.setLocX(3);
            } else if (pX <= (tileSize * 4) + topLeftX && sliderW.getValue() > 3) {
                placeCoordsTemp.setLocX(4);
            } else if (pX <= (tileSize * 5) + topLeftX && sliderW.getValue() > 4) {
                placeCoordsTemp.setLocX(5);
            } else if (pX <= (tileSize * 6) + topLeftX && sliderW.getValue() > 5) {
                placeCoordsTemp.setLocX(6);
            } else if (pX <= (tileSize * 7) + topLeftX && sliderW.getValue() > 6) {
                placeCoordsTemp.setLocX(7);
            } else if (pX <= (tileSize * 8) + topLeftX && sliderW.getValue() > 7) {
                placeCoordsTemp.setLocX(8);
            } else if (pX <= (tileSize * 9) + topLeftX && sliderW.getValue() > 8) {
                placeCoordsTemp.setLocX(9);
            } else if (pX > boardSizePx + topLeftX) {
                placeCoordsTemp.setOutOfBounds(true);
            }

            if (pY <= tileSize * 1 + topLeftY) {
                placeCoordsTemp.setLocY(1);
            } else if (pY <= tileSize * 2 + topLeftY) {
                placeCoordsTemp.setLocY(2);
            } else if (pY <= tileSize * 3 + topLeftY && sliderH.getValue() > 2) {
                placeCoordsTemp.setLocY(3);
            } else if (pY <= tileSize * 4 + topLeftY && sliderH.getValue() > 3) {
                placeCoordsTemp.setLocY(4);
            } else if (pY <= tileSize * 5 + topLeftY && sliderH.getValue() > 4) {
                placeCoordsTemp.setLocY(5);
            } else if (pY <= tileSize * 6 + topLeftY && sliderH.getValue() > 5) {
                placeCoordsTemp.setLocY(6);
            } else if (pY <= tileSize * 7 + topLeftY && sliderH.getValue() > 6) {
                placeCoordsTemp.setLocY(7);
            } else if (pY <= tileSize * 8 + topLeftY && sliderH.getValue() > 7) {
                placeCoordsTemp.setLocY(8);
            } else if (pY <= tileSize * 9 + topLeftY && sliderH.getValue() > 8) {
                placeCoordsTemp.setLocY(9);
            } else if (pY > boardSizePx + topLeftY) {
                placeCoordsTemp.setOutOfBounds(true);
            }
        }


        return placeCoordsTemp;
    }

    public void changeColor(CarColours enumColor1, CarColours enumColor2, CarColours enumColor3, CarColours enumColor4) {
        setPlayerCoords();


        drawPlayer(player1Coords.getLocX(), player1Coords.getLocY(), enumColor1,0);
        drawPlayer(player2Coords.getLocX(), player2Coords.getLocY(),enumColor2,0);
        drawPlayer(player3Coords.getLocX(), player3Coords.getLocY(),enumColor3,0);
        drawPlayer(player4Coords.getLocX(), player4Coords.getLocY(),enumColor4,0);
    }


    /**
     * Setting the width of the board for stuff that I've added
     * @param newWidth
     */
    public void setWidth(int newWidth)  {
        width = newWidth;
    }

    /**
     * Setting the height of the board for stuff that I've added
     * @param newHeight The new height of the board
     */
    public void setHeight(int newHeight)   {
        height = newHeight;
    }


    /**
     * Setting the player coordinates for stuff that I've added
     * It becomes a lot easier to refer to when it's just stored as a number and you don't have to loop through
     * potentially 81 tiles every time to get it
     */
    public void setPlayerCoords()   {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if(arrayBoard[i][j].getWhichPlayer() == 1)  {
                    player1Coords.setLocX(i+1);
                    player1Coords.setLocY(j+1);
                }else if (arrayBoard[i][j].getWhichPlayer() == 2)   {
                    player2Coords.setLocX(i+1);
                    player2Coords.setLocY(j+1);
                }else if (arrayBoard[i][j].getWhichPlayer() == 3)   {
                    player3Coords.setLocX(i+1);
                    player3Coords.setLocY(j+1);
                }else if (arrayBoard[i][j].getWhichPlayer() == 4)   {
                    player4Coords.setLocX(i+1);
                    player4Coords.setLocY(j+1);
                }
            }
        }
    }

    /**
     * Small method that sets the colour value currently being used, there can be a discrepancy between what LEMainController and
     * LECanvas on what the colour is so this is here to rectify that
     * @param P1C Player 1's Colour
     * @param P2C Player 2's Colour
     * @param P3C Player 3's Colour
     * @param P4C Player 4's Colour
     */
    public void setPlayerColours(CarColours P1C, CarColours P2C, CarColours P3C, CarColours P4C)    {
        playerColor1 = P1C;
        playerColor2 = P2C;
        playerColor3 = P3C;
        playerColor4 = P4C;
    }

    //Previous variables for the highlighter method declared here
    Image prevImage;
    double prevX = -1;
    double prevY = -1;
    int prevRotate = -1;
    PlaceCoords prevCoords = new PlaceCoords(-1, -1);

    String oldStatus = "Nothing is Selected";

    /**
     * This method highlights players or tiles when you hover over them in the level editor
     * @param mouseX X position of mouse relative to screen
     * @param mouseY Y position of mouse relative to screen
     * @param sliderW Width slider from LEMainController
     * @param sliderH Height slider from LEMainController
     * @param tileType The type of tile you will be highlighting
     * @param rotate The rotation at which the tile you're highlighting is at
     * @param playerSel If a player is selected, we ignore the code that highlights the player and default back to the tile to signify placement
     * @param theController LEMainController
     */
    public void highlighterMethod(double mouseX, double mouseY, Slider sliderW, Slider sliderH, String tileType, int rotate, Boolean playerSel, LEMainController theController) {
        try {
            //First we set the player Coords
            setPlayerCoords();
            GraphicsContext gc = canvas.getGraphicsContext2D();

            PlaceCoords placeCoordsTemp = new PlaceCoords(0, 0);
            double pX = mouseX;
            double pY = mouseY;
            //checking for out of bounds and tile location
            placeCoordsTemp = checkWhereToPlace((int) tileSize, mouseX, mouseY, sliderW, sliderH);
            //If the mouse if OOB, redraw the tile but only if there's already a previous value there, also reset the variables at the end
            if (placeCoordsTemp.getOutOfBounds()) {
                canvas.setCursor(Cursor.DEFAULT);
                try {
                    drawAThing(gc, prevRotate, prevX, prevY, prevImage);
                    checkForPlayerDraw(prevCoords);
                } catch (Exception e) {

                }
                highlighterReset();
                if (tileType == null && !playerSel)   {
                    lblstatus.setText("Nothing is Selected");
                }else if (playerSel)    {
                  lblstatus.setText("Player " + theController.getIntPlayerSel() + " is Selected");
                } else   {
                    lblstatus.setText(tileType.toLowerCase() + " Tile Selected");
                }
                return;
            }
            //This try catch block is here to catch other OOB cases.
            Boolean rotationChecker = false;
            try {
                belowTileType = arrayBoard[placeCoordsTemp.getLocX() - 1][placeCoordsTemp.getLocY() - 1].getTileType();
                canvas.setCursor(Cursor.HAND);
                if (tileType == null) {
                    rotationChecker = true;
                    tileType = belowTileType.toString();
                    rotate = arrayBoard[placeCoordsTemp.getLocX() - 1][placeCoordsTemp.getLocY() - 1].getRotate();

                } else {
                    lblstatus.setText("Place " + tileType + " on " + placeCoordsTemp.getLocX() + " " + placeCoordsTemp.getLocY());
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                //Another OOB Check, there had to be loads of these to catch all the different 'ways' you could be OOB
                drawAThing(gc, prevRotate, prevX, prevY, prevImage);
                checkForPlayerDraw(prevCoords);
                canvas.setCursor(Cursor.DEFAULT);
                highlighterReset();
                if (tileType == null)    {
                    lblstatus.setText(oldStatus);
                }else   {
                    lblstatus.setText(oldStatus);
                }
                return;
            }

            Boolean boolRotatable = true;
            Image image;
            String imageStr;
            switch (tileType) {
                case "EMPTY":
                    imageStr = "empty";
                    image = Assets.getSelected(imageStr);
                    boolRotatable = false;
                    break;
                case "CORNER":
                    imageStr = "corner";
                    image = Assets.getSelected(imageStr);
                    break;
                case "T_SHAPE":
                    imageStr = "t_shape";
                    image = Assets.getSelected(imageStr);
                    break;
                case "GOAL":
                    imageStr = "goal";
                    image = Assets.getSelected(imageStr);
                    boolRotatable = false;
                    break;
                case "STRAIGHT":
                    imageStr = "straight";
                    image = Assets.getSelected(imageStr);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + tileType);
            }
            double whereXDrawn = ((placeCoordsTemp.getLocX() * tileSize) - tileSize) + topLeftX;
            double whereYDrawn = ((placeCoordsTemp.getLocY() * tileSize) - tileSize) + topLeftY;

            if (prevX != -1) {
                drawAThing(gc, prevRotate, prevX, prevY, prevImage);
                checkForPlayerDraw(prevCoords);
            }
            prevX = whereXDrawn;
            prevY = whereYDrawn;
            prevRotate = arrayBoard[placeCoordsTemp.getLocX() - 1][placeCoordsTemp.getLocY() - 1].getRotate();
            prevImage = Assets.get(belowTileType.toString().toLowerCase());
            prevCoords = placeCoordsTemp;

            //Checking if there's a player on the tile, if so only highlighting the player and not the tile
            //For intuitiveness!
            int playerOnTile = checkForPlayerInt(placeCoordsTemp);
            if (playerOnTile > 0 && rotationChecker && !playerSel) {
                Image imgToDraw;
                switch (playerOnTile) {
                    case 1:
                        imgToDraw = Assets.getSelected("player" + playerColor1.toString());
                        drawAThing(gc, 0, whereXDrawn, whereYDrawn, imgToDraw);
                        lblstatus.setText("Select Player 1/RMB Rotates");
                        return;
                    case 2:
                        imgToDraw = Assets.getSelected("player" + playerColor2.toString());
                        drawAThing(gc, 0, whereXDrawn, whereYDrawn, imgToDraw);
                        lblstatus.setText("Select Player 2/RMB Rotates");
                        return;
                    case 3:
                        imgToDraw = Assets.getSelected("player" + playerColor3.toString());
                        drawAThing(gc, 0, whereXDrawn, whereYDrawn, imgToDraw);
                        lblstatus.setText("Select Player 3/RMB Rotates");
                        return;
                    case 4:
                        imgToDraw = Assets.getSelected("player" + playerColor4.toString());
                        drawAThing(gc, 0, whereXDrawn, whereYDrawn, imgToDraw);
                        lblstatus.setText("Select Player 4/RMB Rotates");
                        return;
                    default:
                        CustomAlerts.Warning("User Alert!", "If you're seeing this it means I messed up some value setting, there's nothing you can do to fix this : (");
                }
            }


            drawAThing(gc, rotate, whereXDrawn, whereYDrawn, image);

            checkForPlayerDraw(placeCoordsTemp);


            //Checking if you have firstly nothing selected, secondly whether or not the tile below is rotatable

            if(!boolRotatable && rotationChecker && !playerSel)   {
                lblstatus.setText(oldStatus);
            }



            if(rotationChecker && boolRotatable && !playerSel) {
                drawAThing(gc, 0, whereXDrawn, whereYDrawn, rotationArrow);
                lblstatus.setText("Rotate " + tileType);
            }

            if(rotationChecker && playerSel)    {
                if (playerOnTile == theController.getIntPlayerSel())    {
                    lblstatus.setText("De-Select Player 1");
                }else if (playerOnTile > 0)  {
                    lblstatus.setText("Can't Place P" + theController.getIntPlayerSel() + " on P" + playerOnTile);
                }else   {
                    lblstatus.setText("Place Player " + theController.getIntPlayerSel() + " on " + placeCoordsTemp.getLocX() + " " + placeCoordsTemp.getLocY());
                }

            }
        }finally    {
            if (!lblstatus.getText().contains("Place") && !lblstatus.getText().contains("Rotate"))  {
                oldStatus = lblstatus.getText();
            }
        }


    }

    /**
     * This method draws a tile (Mostly used in highlighter method because it has to repeat so much)
     * @param gc GraphicsContext from the Canvas object
     * @param rotate The rotation at which you'd like to display your image
     * @param whereXDrawn X coordinate of drawing relative to screen
     * @param whereYDrawn Y coordinate of drawing relative to screen
     * @param imgToDraw The image you wish to draw
     */
    public void drawAThing(GraphicsContext gc, int rotate, double whereXDrawn, double whereYDrawn, Image imgToDraw) {
        //If we somehow get here despite all the checks in the main highlighter method, just fall back.
        if(whereXDrawn == -1)    {
            return;
        }
        gc.save();
        rotateMethod(gc, rotate, whereXDrawn + tileSize / 2, whereYDrawn + tileSize / 2);
        gc.drawImage(imgToDraw, whereXDrawn, whereYDrawn, tileSize, tileSize);
        gc.restore();
    }


    /**
     * Small method to reset all the "prev" values for the highlighter when a board is redrawn (E.G for a resize)
     * Helps stop phantom tiles appearing
     */
    public void highlighterReset()  {
        prevX = -1;
        prevY = -1;
        prevRotate = -1;
        prevCoords = new PlaceCoords(-1, -1);
    }


    /**
     * This method checks if a player is on the tile that is being drawn at the moment and draws the player on top if so
     * @param placeCoordsTemp The coordinates of the tile being drawn
     */
    public void checkForPlayerDraw(PlaceCoords placeCoordsTemp)    {
        setPlayerCoords();
        //Again checking if the value here is a -1 and falling back if it is cause we ain't drawing at -1
        if (placeCoordsTemp.getLocX() == -1) {
            return;
        }


        if (placeCoordsTemp.getLocX() == player1Coords.getLocX() && placeCoordsTemp.getLocY() == player1Coords.getLocY())   {
            drawPlayer(placeCoordsTemp.getLocX(), placeCoordsTemp.getLocY(), playerColor1, 0);
        }else if (placeCoordsTemp.getLocX() == player2Coords.getLocX() && placeCoordsTemp.getLocY() == player2Coords.getLocY()) {
            drawPlayer(placeCoordsTemp.getLocX(), placeCoordsTemp.getLocY(), playerColor2, 0);
        }else if (placeCoordsTemp.getLocX() == player3Coords.getLocX() && placeCoordsTemp.getLocY() == player3Coords.getLocY()) {
            drawPlayer(placeCoordsTemp.getLocX(), placeCoordsTemp.getLocY(), playerColor3, 0);
        }else if (placeCoordsTemp.getLocX() == player4Coords.getLocX() && placeCoordsTemp.getLocY() == player4Coords.getLocY()) {
            drawPlayer(placeCoordsTemp.getLocX(), placeCoordsTemp.getLocY(), playerColor4, 0);
        }
    }

    /**
     * This method checks for a player and returns the int of the player if it finds one (Defaults to -10
     * @param placeCoords The coordinates to check
     * @return The player that is on the tile
     */
    public int checkForPlayerInt(PlaceCoords placeCoords)   {
        setPlayerCoords();
        //Again checking if the value here is a -1 and falling back if it is cause we ain't drawing at -1
        if (placeCoords.getLocX() == -1) {
            return -1;
        }

        if (placeCoords.getLocX() == player1Coords.getLocX() && placeCoords.getLocY() == player1Coords.getLocY())   {
            return 1;
        }else if (placeCoords.getLocX() == player2Coords.getLocX() && placeCoords.getLocY() == player2Coords.getLocY()) {
            return 2;
        }else if (placeCoords.getLocX() == player3Coords.getLocX() && placeCoords.getLocY() == player3Coords.getLocY()) {
            return 3;
        }else if (placeCoords.getLocX() == player4Coords.getLocX() && placeCoords.getLocY() == player4Coords.getLocY()) {
            return 4;
        }else   {
            return 0;
        }
    }

    /**
     * This method handles rotating a tile when nothing is selected
     * Also picking up a player if they are on the tile you click
     * @param mouseX X position of mouse relative to screen
     * @param mouseY Y position of mouse relative to screen
     * @param sliderW Width slider from LEMainController
     * @param sliderH Height slider from LEMainController
     * @param theController LEMainController itself (Used for player selection)
     * @param playerSel Is a player selected currently?
     * @param boolRightClick Is the person right clicking (Rotates the tile below a player)
     */
    public void rotateHoveredTile(double mouseX, double mouseY, Slider sliderW, Slider sliderH, LEMainController theController, Boolean playerSel, Boolean boolRightClick)   {

        GraphicsContext gc = canvas.getGraphicsContext2D();


        PlaceCoords placeCoordsTemp = new PlaceCoords(0, 0);
        double pX = mouseX;
        double pY = mouseY;
        Image image;
        String imageStr;
        //checking for out of bounds and tile location
        placeCoordsTemp = checkWhereToPlace((int) tileSize, mouseX, mouseY, sliderW, sliderH);
        if (placeCoordsTemp.getOutOfBounds()) {
            return;
        }
        int playerOnTile = checkForPlayerInt(placeCoordsTemp);



        String tileType;
        int rotate;
        try {
            tileType = arrayBoard[placeCoordsTemp.getLocX() - 1][placeCoordsTemp.getLocY() - 1].getTileType().toString();
            rotate = arrayBoard[placeCoordsTemp.getLocX()- 1][placeCoordsTemp.getLocY() - 1].getRotate();
        }catch (ArrayIndexOutOfBoundsException e)   {
            return;
        }
        double whereXDrawn = ((placeCoordsTemp.getLocX() * tileSize) - tileSize) + topLeftX;
        double whereYDrawn = ((placeCoordsTemp.getLocY() * tileSize) - tileSize) + topLeftY;

        if(playerOnTile > 0 && !boolRightClick)  {
            switch (playerOnTile)   {
                case 1:
                    theController.onPlayer1();
                    highlighterMethod(mouseX, mouseY, sliderW, sliderH, null, rotate, true, theController);
                    return;
                case 2:
                    theController.onPlayer2();
                    highlighterMethod(mouseX, mouseY, sliderW, sliderH, null, rotate, true, theController);
                    return;
                case 3:
                    theController.onPlayer3();
                    highlighterMethod(mouseX, mouseY, sliderW, sliderH, null, rotate, true, theController);
                    return;
                case 4:
                    theController.onPlayer4();
                    highlighterMethod(mouseX, mouseY, sliderW, sliderH, null, rotate, true, theController);
                    return;
                default:
                    CustomAlerts.Warning("User Alert!", "If you're seeing this it means I messed up some value setting, there's nothing you can do to fix this : (");
                    return;
            }
        }



        switch (tileType) {
            case "EMPTY":
                lblstatus.setText("Can't rotate Empty tiles!");
                if (!playerSel) {
                    return;
                }else   {
                    imageStr = "empty";
                    image = Assets.getSelected(imageStr);
                    break;
                }
            case "CORNER":
                imageStr = "corner";
                image = Assets.getSelected(imageStr);
                break;
            case "T_SHAPE":
                imageStr = "t_shape";
                image = Assets.getSelected(imageStr);
                break;
            case "GOAL":
                lblstatus.setText("Can't rotate Goal tiles!");
                if (!playerSel) {
                    return;
                }else   {
                    imageStr = "goal";
                    image = Assets.getSelected(imageStr);
                    break;
                }
            case "STRAIGHT":
                imageStr = "straight";
                image = Assets.getSelected(imageStr);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + tileType);
        }

        rotate = rotate + 90;
        if (rotate > 270)   {
            rotate = 0;
        }

        arrayBoard[placeCoordsTemp.getLocX() - 1][placeCoordsTemp.getLocY() - 1].setRotate(rotate);

        drawAThing(gc, rotate, whereXDrawn, whereYDrawn, image);
        checkForPlayerDraw(placeCoordsTemp);

        lblstatus.setText("Tile " + placeCoordsTemp.getLocX() +  " " + placeCoordsTemp.getLocY() + " rotated to " + rotate);

    }

}
